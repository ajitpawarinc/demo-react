export const Data = [
  { id:1, name:'diva', title:'diva for life', description:'discription for diva', price:99.00, quantity:4 },
  { id:2, name:'crystal', title:'Clear as water', description:'discription for crystal', price:199.00, quantity:3 },
  { id:3, name:'rock', title:'solid as dimond', description:'discription for diamond', price:999.00, quantity:5 },
  { id:4, name:'box', title:'Full of magic', description:'discription for box', price:79.00, quantity:7 },
];
