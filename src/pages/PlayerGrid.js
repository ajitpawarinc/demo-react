import React, { useState, useEffect, useContext } from 'react';
//simple grid with pagination & filter with sorting
import axios from 'axios';
import * as ReactBootStrap from 'react-bootstrap';

import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from  'react-bootstrap-table2-paginator';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import { DataContext } from '../DataContext';

const PlayerGrid = () => {
    const {User} = useContext( DataContext );

    const [players, setPlayers] = useState([]);
    const [loading, setLoading] = useState(false);

    const getPlayersData = async () => {
        try{
            const  data = await axios.get(
                "https://nba-players.herokuapp.com/players-stats"
            );

            setPlayers(data.data);
            setLoading(true);
        } catch( e ) {
            console.log( e );
        } 
    }

    const columns = [
        {dataField:"name", text:"Player Name", filter:textFilter(), sort:true },
        {dataField:"points_per_game", text:"Points Per Game", filter:textFilter(), sort:true },
        {dataField:"team_name", text:"Player Team", filter:textFilter(), sort:true},
    ]

    useEffect(() => {
        getPlayersData();
    }, []);

    return (
            <div>
            <h2>User:{User}</h2>
            <h2>Grid using API Data</h2>
            {loading ?
                ( <BootstrapTable bootstrap4 keyField="name" data={players} columns={columns} pagination={paginationFactory()} filter={filterFactory()} />) 
                :
                ( <ReactBootStrap.Spinner animation="border"/> )
            }
            </div>
    );

}

export default PlayerGrid;