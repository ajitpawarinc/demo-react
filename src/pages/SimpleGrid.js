import React,{useContext} from 'react'
//simple grid
import * as ReactBootStrap from 'react-bootstrap';
import { Data } from "../Data"
import { DataContext } from '../DataContext';

const SimpleGrid = () => {
    const {User} = useContext( DataContext );

    const arrDataList = Data;

    const renderGrid = ( product, index ) =>{
      return(
        <tr key={index}>
          <td>{product.name}</td>
          <td>{product.title}</td>
          <td>{product.description}</td>
          <td>{product.price}</td>
          <td>{product.quantity}</td>
        </tr> 
      )
    }

    return (
        <div>
          <h2>User:{User}</h2>
        {/*Simple Table Grid*/}
          <ReactBootStrap.Table striped bordered hover>
            <thead>
              <tr>
                <th>Product</th>
                <th>Headline</th>
                <th>Description</th>
                <th>Price</th>
                <th>Quantity</th>
              </tr>
            </thead>
            <tbody>
            {arrDataList.map( renderGrid )}
            </tbody>
          </ReactBootStrap.Table>

        </div>
    )
}

export default SimpleGrid
