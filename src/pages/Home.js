import React from 'react';

const Home = () => {
  const greetMsg = () => {
    return 'Welcome to react.'
}

const greeting = 'Hello!';
const greetObj = { 
    g1:'hi',
    g2:'there !'
}

return(
    <div className='home'>
        <h1>Aj React Demo</h1>
        <br/>

        {/* define constants */}
        <h1>{greeting}</h1> 
        <br/>

        {/*define objects*/}
        <h2>{greetObj.g1} {greetObj.g2}</h2>
        <br/>

        {/*create functions*/}
        <h2>{greetMsg()}</h2>
        <br/>
    </div>
)
}

export default Home;
