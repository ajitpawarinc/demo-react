import React,{useContext} from 'react';
import {DataContext} from '../DataContext';

const Contact = () => {
    const {User} = useContext( DataContext );
    return (
        <div>
            <h1>User:{User}</h1>
            <h2>Contact Number:0123456789</h2>
        </div>
    )
}

export default Contact
