import React,{useState,useContext} from 'react';
import { DataContext } from '../DataContext';
import './RegisterForm.css';

const RegisterForm = () => {
    const {User, setUser} = useContext( DataContext );

    //State object
    const [values,setValues] = useState({
        firstName:"",
        lastName:"",
        email:"",
    });

    const [isSubmit,setIsSubmit] = useState( false );
    const [isValid,setIsValid] = useState( false );

    const handleFnChange = ( e ) =>{
        setValues( { ...values, firstName:e.target.value })
    }
    const handleLnChange = ( e ) =>{
        setValues( { ...values, lastName:e.target.value })
    }
    const handleEmChange = ( e ) =>{
        setValues( { ...values, email:e.target.value })
    }

    const handleSubmit = ( e ) => {
        e.preventDefault();
        setIsSubmit( true );
        if( values.firstName && values.lastName && values.email ){
            setIsValid( true );
            setUser(values.firstName);
            //call for update values api
        }
    }

    return(
        <div className="form-container">
            <h2>User:{User}</h2>
            <form className="register-form" onSubmit={handleSubmit }>  
                { isSubmit && isValid ? <div className="success-message">Thank you for registration.</div> : ''}
                <input
                onChange={handleFnChange}
                value={values.firstName}
                className="form-field"
                placeholder="First Name"
                name="firstname"/>
                { isSubmit && !values.firstName ? <span className='err-span'>Please Enter First Name.</span > : null }
                <input
                onChange={handleLnChange}
                value={values.lastName}
                className="form-field"
                placeholder="Last Name"
                name="lastname"/>
                { isSubmit && !values.lastName ? <span className='err-span'>Please Enter Last Name.</span > : null }
                <input
                onChange={handleEmChange}
                value={values.email}
                className="form-field"
                placeholder="Email"
                name="email"/>
                { isSubmit && !values.email ? <span className='err-span'>Please Enter Email.</span > : null }
                <button class="form-field" type="submit">
                    Register
                </button>
            </form>
        </div>
    );

}

export default RegisterForm;