import React,{useContext} from 'react';
import { DataContext } from '../DataContext';
import { Data } from '../Data';

const ProductList = ( ) => {
    const {User} = useContext( DataContext );
    const ListData = Data;

    return (
        <div>
        <h2>User:{User}</h2>
        {/*define array to list objects | adding key to make unique elements*/}
        {ListData.map( objData => {
            return <div key={objData.id}>Product name: {objData.name}  | Description: {objData.title} </div>;
        })}
        </div>
    );
}

export default ProductList;