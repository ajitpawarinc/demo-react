import React,{useState} from 'react';
import './App.css';
import Navbar from './components/Navbar/Navbar';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import Reports from './pages/Reports';
import ProductList from './pages/ProductList';
import PlayerGrid from './pages/PlayerGrid';
import RegisterForm from './pages/RegisterForm';
import SimpleGrid from './pages/SimpleGrid';
import Contact from './pages/Contact';
import { DataContext } from './DataContext';

function App() {
  const [User, setUser] = useState('Guest')
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <DataContext.Provider value={{User, setUser}}>
            <Route path='/' exact component={Home} />
            <Route path='/reports' component={Reports} />
            <Route path='/products' component={ProductList} />
            <Route path='/samplegrid' component={SimpleGrid} />
            <Route path='/players' component={PlayerGrid} />
            <Route path='/register' component={RegisterForm} />
            <Route path='/support' component={Contact} />
          </DataContext.Provider>
        </Switch>
      </Router>
    </>
  );
}

export default App;
